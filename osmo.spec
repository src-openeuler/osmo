Summary:        Personal organizer
Summary(pl):    Osobisty organizer
Summary(de):    Persönlicher Organizer
Name:           osmo
Version:        0.4.4
Release:        2
License:        GPL-2.0-or-later
Group:          Applications/Productivity
URL:            https://osmo-pim.sourceforge.net/
Source0:        https://downloads.sourceforge.net/%{name}-pim/%{name}-%{version}.tar.gz
Patch0:         backport-Switch-to-webkit2gtk-4.1.patch

BuildRequires: make gcc
BuildRequires: autoconf automake libtool
BuildRequires: pkgconfig(gthread-2.0) >= 2.6.0
BuildRequires: pkgconfig(gtk+-3.0) >= 3.10.0
BuildRequires: pkgconfig(libarchive) >= 3.0.0
BuildRequires: pkgconfig(libical) >= 1.0
BuildRequires: pkgconfig(libnotify) >= 0.7.0
BuildRequires: pkgconfig(libxml-2.0) >= 2.0.0
BuildRequires: pkgconfig(pango) >= 1.20
BuildRequires: pkgconfig(webkit2gtk-4.1) >= 2.8.0
BuildRequires: libgringotts-devel

Requires:       hicolor-icon-theme
Requires:       tzdata
Requires:       /usr/bin/aplay
Requires:       /usr/bin/xdg-email
Requires:       /usr/bin/xdg-open

%description
Osmo is a handy personal organizer which includes calendar, tasks manager and
address book modules. It was designed to be a small, easy to use and good
looking PIM tool to help to manage personal information. In current state the
organizer is quite convenient in use - for example, user can perform nearly
all operations using keyboard. Also, a lot of parameters are configurable to
meet user preferences.

%description -l pl
Osmo to podręczny organizer, zawierający kalendarz, menedżer zadań i książkę
adresową. W zamierzeniu był małym, prostym w obsłudze i dobrze wyglądającym 
menedżerem informacji osobistych. Osmo jest bardzo wygodny - niemal wszystkie
operacje można wykonać za pomocą klawiatury. Program udostępnia wiele opcji,
które użytkownik może zmienić, by program bardziej mu odpowiadał.

%description -l de
Osmo ist ein handlicher persönlicher Organzier mit Kalender, Aufgabenliste und
Adressbuch. Er wurde als kleines, einfach zu benutzendes und gut aussehendes 
PIM-Werkzeug zur Verwaltung persönlicher Informationen entworfen. Im 
gegenwärtigen Zustand ist er sehr angenehm zu benutzen, so kann der Nutzer zum 
Beispiel fast alle Aktionen mit der Tastatur ausführen. Außerdem lassen sich 
viele Parameter einstellen, um die Vorlieben des Benutzers zu treffen.


%prep
%autosetup -p1 -n %{name}-%{version}

%build
autoreconf -vif
%configure --enable-backup=yes --enable-printing=yes \
  --with-contacts --with-tasks --with-notes
%make_build

%install
%make_install

# icon
mv %{buildroot}%{_datadir}/pixmaps/%{name}.png \
  %{buildroot}%{_datadir}/icons/hicolor/48x48/apps

# Remove empty directory.
rm -rf %{buildroot}%{_datadir}/pixmaps/

%find_lang %{name}

%files -f %{name}.lang
%license COPYING
%doc AUTHORS ChangeLog README TRANSLATORS
%{_bindir}/%{name}
%{_datadir}/applications/*%{name}.desktop
%{_datadir}/icons/*/*/*/*
%{_mandir}/man1/%{name}.1*
%{_datadir}/sounds/osmo

%changelog
* Sun Dec 01 2024 Funda Wang <fundawang@yeah.net> - 0.4.4-2
- Use webkitgtk-4.1
- cleanup spec

* Wed May 24 2023 wangtaozhi <wangtaozhi@kylinsec.com.cn> - 0.4.4-1
- Package init
